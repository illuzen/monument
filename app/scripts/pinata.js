const pinataSDK = require('@pinata/sdk');
require('dotenv').config() // Store environment-specific variable from '.env' to process.env
const fs = require('fs')

const pinata = pinataSDK(
  process.env.REACT_APP_PINATA_API_KEY,
  process.env.REACT_APP_PINATA_SECRET_API_KEY
);

//await pinata.pinJSONToIPFS(body);
// or
const filename = process.argv[2]
console.log('Attempting to pin ', filename, ' to IPFS via pinata')
//const rs = fs.createReadStream(filename);
pinata.pinFromFS(filename).then(result => console.log({result})).catch(err => console.error({err}))
