const pinataSDK = require('@pinata/sdk');
require('dotenv').config() // Store environment-specific variable from '.env' to process.env
const fs = require('fs')

const pinata = pinataSDK(
  process.env.REACT_APP_PINATA_API_KEY,
  process.env.REACT_APP_PINATA_SECRET_API_KEY
);

//await pinata.pinJSONToIPFS(body);
// or
const d = require('./reset.json')


const promise = (filename) => new Promise((resolve, reject) => {
    pinata.pinFromFS(filename).then(result => {
//        console.log({result})
        resolve(result)
    }).catch(async err => {
//        console.error({err})
        console.log('Error uploading, trying again')
        await promise(filename)
//        reject(err)
    })
})

async function f() {
    let i = 0
    const collectionLength = 100
    while (i < d.length) {
        const d2 = {entries: {}}
        for (let j = 0; j < collectionLength; j++) {
            if (i >= d.length) {
                break;
            }
            const imagePath = d[i]['image_path']
            if (imagePath == '') {
                continue;
            }
            const imageFilename = '../' + imagePath
            const pdfFilepath = d[i]['file_path']
            const pdf_name = pdfFilepath.split('/').pop()
    //        console.log({pdf_name})
    //        console.log(d[i]['file_path'].split('/').pop())
            console.log('Working on item ' + i)
            console.log('Attempting to pin ', imageFilename, ' to IPFS via pinata')
            const result = await promise(imageFilename)
//            console.log({result})
            d[i]['image'] = result.IpfsHash
            console.log('Attempting to pin ', pdfFilepath, ' to IPFS via pinata')
            const result2 = await promise(pdfFilepath)
            d[i]['hash'] = result2.IpfsHash

            d2['entries'][pdf_name] = {
                'image': result.IpfsHash,
                'hash': result2.IpfsHash,
                'description': d[i]['description'],
                'title': d[i]['title']
            }
            i++

        }
        const file_out = './reset-image-' + i + '.json'
        const output = JSON.stringify(d2)
        fs.writeFileSync(file_out, output)
        const result3 = await promise(file_out)
        console.log({result3})

    }
}

async function f2() {
    const a = [
//        './scripts/collection-1.json',
//        './scripts/collection-2.json',
//        './scripts/collection-3.json',
//        './scripts/collection-4.json',
//        './scripts/collection-5.json',
        './reset-image-100.json',
        './reset-image-200.json',
        './reset-image-300.json',
        './reset-image-400.json',
        './reset-image-482.json',
//        './reset-image-500.json',
//        './reset-image-600.json',
//        './reset-image-700.json',
//        './reset-image-800.json',
//        './reset-image-900.json',
//        './reset-image-973.json',
    ]
    a.forEach(async path => {
        const result = await promise(path)
        console.log({result})
    })

}

(async function ff() {
    await f()
    await f2()
})()
