import { useState } from 'react'
import { Input, Button, Upload } from 'antd';

const Annotate = ({name, setTitle, setDescription, setImage}) => {
    const [imageSet, setImageSet] = useState(false)
    const handleUpload = (info) => {
        console.log({info})
        if (info.file.status !== 'uploading') {
            console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
            console.log(`${info.file.name} file uploaded successfully`);
            setImage(info.file.response.IpfsHash)
        } else if (info.file.status === 'error') {
          console.error(`${info.file.name} file upload failed.`);
        }
    }

    return (
        <div>
          <Upload
            name='file'
            listType='picture-card'
            accept='image/png'
            action={'https://api.pinata.cloud/pinning/pinFileToIPFS'}
            headers={{
                pinata_api_key: process.env.REACT_APP_PINATA_API_KEY,
                pinata_secret_api_key: process.env.REACT_APP_PINATA_SECRET_API_KEY,
            }}
            showUploadList={true}
            multiple={false}
            maxCount={1}
            onChange={handleUpload}
          > {'+ Image'}
          </Upload>
          <Input
            placeholder={'title'}
            onChange={(e) => setTitle(e.target.value)}
          />
          <Input
            placeholder="description"
            onChange={(e) => setDescription(e.target.value)}
          />
        </div>
    )
}

export default Annotate