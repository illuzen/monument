import { Input, Button, Upload, Card } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { useState } from 'react'
import { uploadIPFS, addContent } from './contract'
import Annotate from './Annotate'

// totalState = { name: '', entries: { filename: { title, description, hash }

const UploadIPFS = () => {

  const [totalState, setTotalState] = useState({
    entries: {}, timestamp: new Date().getTime()
  })

  const handleUpload = (info) => {
    if (info.file.status !== 'uploading') {
    }
    if (info.file.status === 'done') {
        setTotalState(prevState => {
            const newState = {...prevState}
            if (newState.entries[info.file.name]) {
                newState.entries[info.file.name].hash = info.file.response.IpfsHash
            } else {
                newState.entries[info.file.name] = { hash: info.file.response.IpfsHash }
            }
            return newState
        })
    } else if (info.file.status === 'error') {
      console.error(`${info.file.name} file upload failed.`);
    }
  }

  const setTitle = (title, file) => {
    setTotalState(prevState => {
        const newState = {...prevState}
        if (newState.entries[file.name]) {
            newState.entries[file.name].title = title
        } else {
            newState.entries[file.name] = { title }
        }
        return newState
    })
  }

  const setDescription = (description, file) => {
    setTotalState(prevState => {
        const newState = {...prevState}
        if (newState.entries[file.name]) {
            newState.entries[file.name].description = description
        } else {
            newState.entries[file.name] = { description }
        }
        return newState
    })
  }

  const setImage = (image, file) => {
    setTotalState(prevState => {
        const newState = {...prevState}
        if (newState.entries[file.name]) {
            newState.entries[file.name].image = image
        } else {
            newState.entries[file.name] = { image }
        }
        return newState
    })
  }

  const itemRender = (x, y) => {
    return (
        <Card title={x}>
            <Annotate
                name={y.name}
                setTitle={t => setTitle(t,y)}
                setDescription={d => setDescription(d,y)}
                setImage={i => setImage(i, y)}
            />
        </Card>
    )
  }

  const beforeUpload = (x) => {
    return true
  }

  const onPreview = (x) => {
  }

  const onClick = () => {
    uploadIPFS(totalState, (hash) => {
        addContent(hash)
    })
  }

  const onRemove = (x) => {
    setTotalState(prevState => {
        const newState = {...prevState}
        delete newState.entries[x.name]
        return newState
    })
    return true
  }

  return (
    <div className='Upload'>
        <Button
            type="primary"
            onClick={onClick}
            // TODO: make this better
            disabled={Object.keys(totalState).length < 1}
        >
        Submit
        </Button>
      <Upload
        name='file'
        accept='application/pdf'
        action={'https://api.pinata.cloud/pinning/pinFileToIPFS'}
        directory
        headers={{
            pinata_api_key: process.env.REACT_APP_PINATA_API_KEY,
            pinata_secret_api_key: process.env.REACT_APP_PINATA_SECRET_API_KEY,
        }}
        beforeUpload={beforeUpload}
        onChange={handleUpload}
        itemRender={itemRender}
        onPreview={onPreview}
        onRemove={onRemove}
      >
        <Button icon={<UploadOutlined />}>Click to Upload Directory of PDFs</Button>
      </Upload>
    </div>
  );
};

export default UploadIPFS;