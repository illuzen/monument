import { ethInstance, ipfsHashToBytes32 } from 'evm-chain-scripts';
const pinataSDK = require('@pinata/sdk');

const pinata = pinataSDK(
  process.env.REACT_APP_PINATA_API_KEY,
  process.env.REACT_APP_PINATA_SECRET_API_KEY
);

const ContentRegistry = require('./abi/ContentRegistry.json')
const WriterNFT = require('./abi/WriterNFT.json')
//const contractOptions = {chainId: 4}
const contractOptions = {chainId: 1}

export function shuffleArray(arr) {
  arr.sort(() => Math.random() - 0.5);
}


export async function isWriter() {
  const registry = await ethInstance.getContract('read', ContentRegistry, contractOptions)
  const nft = await ethInstance.getContract('read', WriterNFT, contractOptions)
  const account = await ethInstance.getEthAccount(false)
  const writer = await registry.methods.eligibleWriter(account, nft._address).call()
  return writer
}

export async function getAllContent(processEvent) {
  const registry = await ethInstance.getContract('read', ContentRegistry, contractOptions)
  const nft = await ethInstance.getContract('read', WriterNFT, contractOptions)
  const contents = [...await registry.methods.getContent(nft._address).call()]
  shuffleArray(contents)
  contents.forEach(async content => {
      processEvent(content)
  })
//  registry.methods.topics
}

export async function addContent(hash) {
  const bytes = ipfsHashToBytes32(hash)
  const registry = await ethInstance.getContract('write', ContentRegistry, contractOptions)
  const nft = await ethInstance.getContract('read', WriterNFT, contractOptions)
  const account = await ethInstance.getEthAccount()
  await registry.methods.addContent(nft._address, bytes).send({ from: account, gas: 200000 })
}

export async function createNFT(recipient) {

}

export function uploadIPFS(state, callback) {
  //await pinata.pinJSONToIPFS(body);
  //const rs = fs.createReadStream(filename);
  //  pinata.pinFromFS(filename).then(result => console.log({result})).catch(err => console.error({err}))
  pinata.pinJSONToIPFS(state).then(result => {
    callback(result.IpfsHash)
  }).catch(err => console.error({err}))
}

const IPFS_GATEWAYS = [
    "cloudflare-ipfs.com",
    "cf-ipfs.com",
    "ipfs.io",
    "ipfs.fleek.co",
    "dweb.link",
    "gateway.pinata.cloud",
    "ipfs.infura.io"
]

export function getMirrors(ipfsHash) {
//    shuffleArray(IPFS_GATEWAYS)
    return IPFS_GATEWAYS.map(x => `https://${x}/ipfs/${ipfsHash}`)
}

export async function ipfsGet(ipfsHash, failoverCallback) {
    for (let i = 0; i < IPFS_GATEWAYS.length; i++) {
        try {
            const url = `https://${IPFS_GATEWAYS[i]}/ipfs/${ipfsHash}`;
            const res = fetch(url)
            return res
        }
        catch (er) {
            // console.error(er);
            // try another gateway
        }
    }
    return failoverCallback ? failoverCallback() : false;
}
