import './App.css';
import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
import { PageHeader, Input, Button } from 'antd';
import { getAllContent, getAllTopics, ipfsGet, isWriter, addContent, getMirrors } from './contract'
import UploadIPFS from './Upload'
import { useState, useEffect } from 'react'
import { bytes32ToIpfsHash, ipfsHashToBytes32, ConnectWallet, eventListeners } from 'evm-chain-scripts';
import Collection from './Collection'
const { Search } = Input;


function App() {

    const [data, setData] = useState([])
    const entryThreshold = 1000
    const [writer, setWriter] = useState(false)
    const processContent = async x => {

        const hash = bytes32ToIpfsHash(x)
    //            console.log("ipfsHash: ", hash)
        const contentStream = await ipfsGet(hash, () => { console.error('Failed to get data from IPFS'); })
        const content = await contentStream.json()
    //            console.log("content: ", content)

        setData(prevState => {
            const obj = Object.values(content.entries)
    //                console.log('entries', obj)
            obj.forEach(x => {
                x.mirrors = getMirrors(x.hash)
                x.link = x.mirrors.pop()
                if (x.image) {
                    x.imageMirrors = getMirrors(x.image)
                    x.imageLink = x.imageMirrors.pop()
                }
            })
            return [...prevState, content]
        })
    }

    useEffect(async () => {

        getAllContent(processContent)
        eventListeners.addEventListener(eventListeners.EVENTS.NETWORK_CHANGE, async () => {
            const w = await isWriter()
            setWriter(w)
        })
//        function f() {
//            addContent('QmZA5CHAJuAMnGkHCX5cz9fxqqhJ1kK4oFaD27dSe9z3WA')
//            addContent('QmaMwUP4HDzaeWTTF7djZMH1GTkmhWfekuAUy3ZvuQFRoY')
//            addContent('QmWg88EXD4eB8WUt9sxZV8TNCfR3wjXFZjL15wwFejq1tG')
//            addContent('QmQ5kgpNEgioyUbcUARGe9EAScsncmMoCjZWeuaXmP7qyY')
//            addContent('QmYEe4gUg9mC4FvkmTNcEevdWeUHAfaq6Rbw7mdF1sPWJq')
//        }
//        setTimeout(f, 1000)
    }, [])

    const onSearch = x => {
        const term = x.target.value.toLowerCase()
        setData(prevState => {
            const newState = [...prevState]
            newState.forEach(collection => {
                Object.keys(collection.entries).forEach(filename => {
                    const entry = collection.entries[filename]
                    if (entry.title.toLowerCase().indexOf(term) < 0 && entry.description.toLowerCase().indexOf(term) < 0) {
                        entry.hidden = true
                    } else {
                        entry.hidden = false
                    }
                })
            })
            return newState
        })
    }

  function dedupe(d) {
    const seen = {}
    d.forEach(x => {
        seen[x.title] = x
    })
    return Object.values(seen)
  }

  return (
    <div className="App">
      <PageHeader
        title={
            <div>
                <img width={'50px'} src={'./logo-150x150.jpg'} className={'logo'}/>
                resetme.eth
            </div>
        }
      />
      <ConnectWallet />
      <Input
        style={{ marginLeft: '20%', width: '60%', marginBottom: '20px' }}
        placeholder="Filter articles by searching here"
        onChange={onSearch}
      />
      { writer ? <UploadIPFS /> : null }
      { dedupe(data).length > 0 ? <Collection entries={dedupe(data)[0].entries} /> : null }
    </div>
  );
}

export default App;
