import { List } from 'antd';
import { useState } from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import { shuffleArray } from './contract';
import './App.css';

function shorten(text) {
    return text.slice(0, 222) + '...'
}

function VisibilityItem ({ item }) {
    const [visibility, setVisible] = useState(false)

    return (
        <VisibilitySensor
            partialVisibility
            onChange={(isVisible) => {
                if (isVisible) {
                    setVisible(isVisible)
                }
//                console.log('viz', item.imageLink, isVisible)
            }}
        >
            <a href={item.link} className='item' target="_blank" rel="noopener noreferrer">
            { visibility ?
                (
                    <div>
                    { item.imageLink ?
                        <img
                            width={'50%'}
                            src={item.imageLink}
                            className='listImage'
                        />
                      : null
                    }

                    </div>
                ) : null
            }
            <List.Item
                style={{marginBottom: 100}}
                extra={
                    <div className={"mirrors"}>
                        { item.mirrors.map((x, i) => <a key={i} href={x} target="_blank" rel="noopener noreferrer">{"Mirror " + i + " "}</a>) }
                    </div>
                }>
                <List.Item.Meta
                    title={<strong className={'articleTitle'}>{item.title}</strong>}
                    description={<a href={item.link} target="_blank" rel="noopener noreferrer">{shorten(item.description)}</a>}
                />
            </List.Item>
            </a>
        </VisibilitySensor >
    )
}

function Collection ({entries}) {
    const ent = Object.values(entries)
    shuffleArray(ent)
    return (
        <div>
          <List
            className="List"
            itemLayout="horizontal"
            dataSource={ent}
            bordered={true}
            style={{ marginLeft: '20%', marginRight: '20%'}}
            grid={{gutter: 1, column: 1}}
            renderItem={item => {
                if (item.hidden) {
                    return null;
                }
                return (
                    <VisibilityItem item={item} />
                )
            } }
          />
          </div>
    )
}

export default Collection