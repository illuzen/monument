# Monument

Inspired by the [Georgia Guidestones](https://en.wikipedia.org/wiki/Georgia_Guidestones) and other memories etched in stone, this software is meant to facilitate the preservation of important information. It is offered free of charge, under the MIT software license. 

### Installation

You will need to install [Brownie](https://eth-brownie.readthedocs.io/etps://eth-brownie.readthedocs.io/en/stable/index.html), a python-based development environment. You will also need yarn or npm to run the webapp, which lives in the `app` folder.

#####Compile and deploy the contracts
`brownie compile`<br/> 
`brownie run scripts/deploy.py`<br/> 

#####Install and run the webapp
`cd app`<br/>
`npm install` <br/>
`npm run start` <br/>


### Design

This was designed to be minimalist, focusing on simplicity for the end user. Any organization of the content is done off-chain, to allow for flexibility. The system is pdf based because this is a very portable and flexible document format. It would be easy to fork the system to support other formats, such as video. NFTs are used, but only to represent write-access to the database. If you hold one of the WriterNFTs, you can call addContent in ContentRegistry, otherwise, you can only read from it. Different NFTs get different tables in the database, meaning the contentMap is indexed by nft contract address. This allows multiple monument-based sites to all use the same database if they want. Each bytes32 hash in the contentMap represents an IPFS hash that represents a json object with a list of hashes of pdfs along with some metadata for those pdfs. The boundary between different bytes32 hashes in the contract is not considered important for the user, so it is not exposed on the frontend. The user just sees a searchable list of pdfs, without knowing how they were grouped on upload. 

### Collaboration

Pull requests are welcome, particularly regarding improvements to the visual design or user experience, or increased modularity to make it easier for others to build on top of it. 