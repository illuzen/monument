import json
from glob import glob
import re
import fitz  # this is pymupdf
import csv
import requests
from os.path import exists
from PIL import Image
import io

# file_path = '/home/metapriest/Documents/RESET_NFTs/3 Tips For Talking To Your Relatives About Marijuana Reform This Holiday Season - Reset.me.pdf'
# file_path = '/home/metapriest/Documents/RESET_NFTs/3 Trippy Ideas From A Radical Pioneer Of Psychedelic Psychiatry - Reset.me.pdf'



def extract_description(file_path):
    # reader = PyPDF2.PdfFileReader(open(file_path, 'rb'))
    # first_page = reader.getPage(0)
    # txt = first_page.extractText()

    doc = fitz.open(file_path)
    first_page = doc[0]

    txt = first_page.get_text()
    # print(txt)
    search = re.search(r'\n“*([A-Z])\n', txt)
    if search is None:
        print('huh ')
        first_letter = ''
    else:
        first_letter = search.group(1)
        if len(first_letter) != 1:
            print('uh oh')
    s = re.split(r'\n“*[A-Z]\n', txt)
    if len(s) == 1:
        s = txt.split('Related Posts\n')
    #     s = re.split('\w[A-Z]\n', txt)
    # elif s[1][1] != '\n':
    title = s[1].split('|')[0].replace('\n', ' ').strip()
    if len(title) > 103:
        print('uh oh')
        s = txt.split('Related Posts\n')
        if len(s) == 1:
            s = txt.split('Stories /')
            title = s[1].split('\n')[0]
        else:
            title = s[0].split('/')[-1].replace('\n', ' ').strip()
    print('Title: {}'.format(title))

    # get biggest image
    d = first_page.get_text("dict")
    blocks = d["blocks"]
    imgblocks = [b for b in blocks if b["type"] == 1]
    biggest_image = sorted(imgblocks, key=lambda x: x['width'] * x['height'] * len(x['image']))[-1]
    # biggest_image = sorted(imgblocks, key=lambda x: len(x['image']))[-1]
    # biggest_image = sorted(imgblocks, key=lambda x: x['width'] * x['height'])[-1]
    # if len(biggest_image['image']) < 60000:
    #     print('Image too small, swapping sorting method: {}'.format(title))
    #     biggest_image = sorted(imgblocks, key=lambda x: x['size'])[-1]
    #     # biggest_image = sorted(imgblocks, key=lambda x: len(x['image']))[-1]
    image_path = './data/header_images/{}.png'.format(title.replace('/', '-'))
    Image.open(io.BytesIO(biggest_image['image'])).save(image_path)


    s = txt.split('COMMENT\n')
    if len(s) == 1:
        s = txt.split('COMMENTS\n')
        if len(s) == 1:
            s = re.split(r'BY .*\n', txt)
    first_paragraph = s[1]

    if '[Edit' in first_paragraph[:100]:
        first_paragraph = first_paragraph.split(']\n')[1]
    first_paragraph = ' '.join(first_paragraph.split('\n')[:16]) + '..'
    return first_letter + first_paragraph, title, image_path


def process_pdfs(doc_dir):
    d = []
    for file_path in glob('{}*.pdf'.format(doc_dir)):
        # if 'Essential Oils Could' not in file_path:
        #     continue
        desc, title, image_path = extract_description(file_path)
        o = {
            'file_path': file_path,
            'description': desc,
            'hash': '',
            'image': '',
            'title': title,
            'image_path': image_path
        }
        d.append(o)
        # print('{} {}'.format(o['file_path'], o['title']))

    return d


# csv_path = './RESET2-2021-October-26-2353.csv'

# doc_dir = '/home/metapriest/Documents/RESET_NFTs/'
doc_dir = '/home/metapriest/Documents/ResetOriginals/'
img_dir = './data/images/'

# img_map = process_csv()
d = process_pdfs(doc_dir)

# print(d)
json.dump(d, open('./reset.json', 'w'))

# finish by using app/scripts/uploadPinata

