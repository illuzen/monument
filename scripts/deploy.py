from brownie import ContentRegistry, WriterNFT, SafeMathLib, accounts, network
import shutil
import json


print('network', network.chain.id)

def move_json():
    deployment_map = json.load(open('./build/deployments/map.json', 'r'))
    for contract_name in ['ContentRegistry', 'WriterNFT']:
        with open('./build/contracts/{}.json'.format(contract_name)) as f:
            j = json.load(f)
            j['networks'] = {
                "4": {
                    "address": deployment_map["4"][contract_name][0]
                },
                "333": {
                    "address": deployment_map['dev'][contract_name][0]
                }
            }
            json.dump(j, open('./app/src/abi/{}.json'.format(contract_name), 'w'), indent=4)


def main():
    if network.chain.id not in [333, 1337]:
        acct = accounts.load('deployment')
    else:
        acct = accounts[0]
    SafeMathLib.deploy({'from': acct})
    nft = WriterNFT.deploy(acct, acct, 'PlantKnowledgeWriter', 'PLANT', {'from': acct})
    registry = ContentRegistry.deploy(acct, {'from': acct})

    nft.createIdentityFor(acct, 1, '0x')
    registry.addNFT(nft, {'from': acct})
    # registry.addContent('0x', '0x', '0x', '0x1')

    move_json()